using lab2_weather;
using Microsoft.AspNetCore.Mvc;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

int counter = 0;
List<Weather> weathers = new List<Weather>();

app.MapGet("weather", () => weathers);
app.MapPost("weather", (Weather weather) =>
{
    weather.Id = counter;
    weathers.Add(weather);
    counter++;
});

app.MapPut("weather", (Weather weather) =>
{
    weathers.Remove(weathers.Find(w => w.Id == weather.Id));
    weathers.Add(weather);
});

app.MapDelete("weather", ([FromBody] Weather weather) => weathers.Remove(weathers.Find(w => w.Id == weather.Id)));

app.Run("http://localhost:4545");