﻿namespace lab2_weather;

public class Weather
{
    public int Id { get; set; }
    public string City { get; }
    public int Temperature { get; }
    public string Wind { get; }
    public string Precipitation { get; }

    public Weather(string city, int temperature, string wind, string precipitation)
    {
        Id = 0;
        City = city;
        Temperature = temperature;
        Wind = wind;
        Precipitation = precipitation;
    }
}